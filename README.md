TryEngine - GDD
=============

> [**Voir le GDD sur Google Doc**](http://derpy.me/GDD "lien vers le GDD")

#### **1. A propos du projet**#
	- Le but du projet
	- Pourquoi un tel projet

#### **2. Cahiers des charges**#
	- Le moteur de jeu
	- Le jeu

------

### **1. A propos du projet**#

###### Le but du projet #
Développer un moteur de jeu axé sur la production en parallèle d’un jeu en 2D (Padmaster). L'objectif serait d'obtenir une démo fonctionnelle du jeu et une base réutilisable pour nos futures productions. 
Le moteur est conçus sur une base [openGL](https://www.opengl.org/) et est codé à l’aide de bibliothèques telles que la [SFML2.2](http://www.sfml-dev.org/index-fr.php).
Le jeu se veut open-source, vous avez donc accès à l'ensemble du code source et des ressources utilisées pour mener à bien le projet.

###### pourquoi un tel projet  #
Ce projet a pour but de nous apprendre à gérer un projet en équipe. Il est développé sur notre temps libre et nous avons donc pour vocation d’apprendre plus de choses encore.
Ce projet est aussi un moyen de prouver notre motivation et notre sérieux si l’occasion s’y prêtait. 

*Un des pourquoi du projet est aussi dû au fait que nous somme tous joueurs chacun de notre côté et motivé à mettre en commun nos compétences pour réaliser ensemble un petit projet pour s’amuser.*

### **2. Cahier des charges**#

#### **2.1. Le moteur de jeu**#
L'élément principale du projet sur quoi tout repose est le moteur de jeu. Celui-ci doit pouvoir fonctionner pour tous les cas d'utilisations dont nous pourrions avoir besoin pour développer un jeu 2D. Ce moteur est un moteur 2D et ne convient qu'à la conception de jeu 2D bien spécifiques (shooters).

#### **2.2. Padmaster**#
_Padmaster est le jeu vidéo test pour lequel est développé le moteur de jeu, il sert aussi de sandbox pour tester et développer notre moteur de jeu. Le jeu est encore à l'état de réflexion ainsi le cahier des charges est encore en pleine conception._
voir [Padmaster sur github](https://github.com/teroratsu/Padmaster).
